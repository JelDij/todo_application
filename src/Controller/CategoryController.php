<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CreateCategoryType;
use App\Repository\CategoryRepository;
use App\Repository\TaskRepository;
use App\Service\CategoryService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/category')]
class CategoryController extends AbstractController
{
    #[Route('/', name: 'overview')]
    public function index(CategoryService $categoryService): Response
    {
        $categories = $categoryService->getAllCategories($this->getUser());
        return $this->render('category/index.html.twig', [
            'categories' => $categories
        ]);
    }

    #[Route('/show/{id}', name: 'show')]
    public function show($id) {
        return $this->redirectToRoute('taskoverview', [
            'id' => $id]);
    }

    /**
     * Method creates a new Category for the current logged in user using CreateCategoryType.php form
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    #[Route('/create', name: 'create')]
    public function create(Request $request) {
        $category = new Category();
        $form = $this->createForm(CreateCategoryType::class, $category);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $user = $this->getUser();
            $category->setUser($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            return $this->redirect('/category');
        }
        return $this->render('category/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route('/delete/{id}', name: 'delete')]
    public function remove(Category $category) {
        if ($category->getTask()->count() == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush();
            $this->addFlash('success', 'Category was deleted');
            return $this->redirect('/category');
        } else {
            $this->addFlash('error', 'Category is not empty');
            return $this->redirect('/category');
        }


    }
}
