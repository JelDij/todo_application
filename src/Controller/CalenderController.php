<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Repository\TaskRepository;
use App\Repository\TodoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class CalenderController extends AbstractController
{
    #[Route('/calender', name:'calender')]
    public function calender(CategoryRepository $categoryRepository, TaskRepository $taskRepository, TodoRepository $todoRepository): Response
    {
        $categories = $categoryRepository->findBy(['user' => $this->getUser()]);
        $tasks = $taskRepository->findBy(['category' => $categories]);
        $todos = $todoRepository->findBy(['task' => $tasks]);
        // by merging the two arrays we can sort them using twig and call the to string to display the name/description
        $collection = array_merge($tasks, $todos);
        return $this->render('calender/index.html.twig', [
            'collection' => $collection,
        ]);
    }
}
