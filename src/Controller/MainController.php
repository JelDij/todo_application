<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[Route("/", name: "home")]
    public function index()
    {
        if ($this->getUser()) {
            return $this->render('home/index.html.twig',
            ['name' => $this->getUser()->getUsername()]);
        }
        return $this->render("home/index.html.twig");
    }
}
