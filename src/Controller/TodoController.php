<?php

namespace App\Controller;

use App\Entity\Task;
use App\Entity\Todo;
use App\Form\CreateTodoType;
use App\Repository\TaskRepository;
use App\Repository\TodoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/todo', name: 'todo')]
class TodoController extends AbstractController
{
    #[Route('/{id}', name: 'overview')]
    public function overviewTodo($id, TaskRepository $taskRepository, TodoRepository $todoRepository): Response
    {
        $task = $taskRepository->findOneBy(['id' => $id]);
        $todos = $todoRepository->findBy(['task' => $task]);
        return $this->render('todo/index.html.twig', [
            'task' => $task,
            'todos' => $todos,
        ]);
    }

    #[Route('/create/{id}', name: 'create')]
    public function createTodo(\Symfony\Component\HttpFoundation\Request $request, Task $task) {
        $todo = new Todo();
        $todo->setTask($task);
        $form = $this->createForm(CreateTodoType::class, $todo);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($todo);
            $em->flush();
            return $this->redirectToRoute('todooverview', [
                'id' => $todo->getTask()->getId()
            ]);
        }
        return $this->render('todo/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route('/delete/{id}', name: 'delete')]
    public function remove(Todo $todo) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($todo);
        $em->flush();
        $this->addFlash('success', 'Todo was deleted');
        return $this->redirectToRoute('todooverview', [
            'id' => $todo->getTask()->getId()
        ]);
    }

    #[Route('/complete/{id}', name: 'complete')]
    public function complete(Todo $todo) {
        if ($todo->getCompleted() == false) {
            $todo->setCompleted(true);
        } else {
            $todo->setCompleted(false);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($todo);
        $em->flush();
        return $this->redirectToRoute('todooverview', [
            'id' => $todo->getTask()->getId()
            ]);
    }
}
