<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Task;
use App\Form\CreateTaskType;
use App\Repository\CategoryRepository;
use App\Repository\TaskRepository;
use App\Repository\TodoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use function Symfony\Component\Translation\t;


#[Route('/task', name: 'task')]
class TaskController extends AbstractController
{
    #[Route('/{id}', name: 'overview')]
    public function overviewTask($id, CategoryRepository $categoryRepository, TaskRepository $taskRepository): Response
    {
        $category = $categoryRepository->findOneBy(['id' => $id]);
        $tasks = $taskRepository->findBy(['category' => $category]);
        return $this->render('task/index.html.twig', [
            'category' => $category,
            'tasks' => $tasks,
        ]);
    }

    #[Route('/create/{id}', name: 'create')]
    public function createTask(Request $request, Category $category)
    {
        $task = new Task();
        $task->setCategory($category);
        $form = $this->createForm(CreateTaskType::class, $task);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();
            return $this->redirectToRoute('taskoverview', [
                'id' => $task->getCategory()->getId()
            ]);
        }
        return $this->render('task/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route('/delete/{id}', name: 'delete')]
    public function remove(Task $task, TodoRepository $todoRepository)
    {
        $todos = $todoRepository->findBy(['task' => $task]);
        $taskIsCompleted = $this->checkIfTodosAreCompleted($todos);
        // if task is completed, it can be deleted along with the subtodo's
        if($taskIsCompleted == true) {
            $em = $this->getDoctrine()->getManager();
            foreach($todos as $value) {
                $em->remove($value);
                $em->flush();
            }
            $em->remove($task);
            $em->flush();
            $this->addFlash('success', 'Task was deleted');
            return $this->redirectToRoute('taskoverview', [
                'id' => $task->getCategory()->getId()
                ]);
        }
        // if task is empty, it can be deleted
        if ($task->getTodo()->count() == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($task);
            $em->flush();
            $this->addFlash('success', 'Task was deleted');
        } else {
            $this->addFlash('error', 'Task is not completed');

        }
        return $this->redirectToRoute('taskoverview', [
            'id' => $task->getCategory()->getId()
        ]);
    }

    /**
     * @param array $todos
     * @return array
     */
    public function checkIfTodosAreCompleted(array $todos): bool
    {
        $boolean = true;
        foreach ($todos as $value) {
            if ($value->getCompleted() == false) {
                $boolean = false;
            }
        }
        return $boolean;
    }
}
