<?php

namespace App\Entity;

use App\Repository\TaskRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TaskRepository::class)
 */
class Task
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $deadline;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="task")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="Todo", mappedBy="task")
     */
    private $todo;

    public function __construct()
    {
        $this->todo = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getName();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDeadline(): ?\DateTimeInterface
    {
        return $this->deadline;
    }

    public function setDeadline(?\DateTimeInterface $deadline): self
    {
        $this->deadline = $deadline;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|Todo[]
     */
    public function getTodo(): Collection
    {
        return $this->todo;
    }

    public function addTodo(Todo $todo): self
    {
        if (!$this->todo->contains($todo)) {
            $this->todo[] = $todo;
            $todo->setTask($this);
        }

        return $this;
    }

    public function removeTodo(Todo $todo): self
    {
        if ($this->todo->removeElement($todo)) {
            // set the owning side to null (unless already changed)
            if ($todo->getTask() === $this) {
                $todo->setTask(null);
            }
        }

        return $this;
    }
}
