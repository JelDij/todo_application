all: up down

up:
	docker-compose --env-file='./.env.local' up -d

down:
	docker-compose down
